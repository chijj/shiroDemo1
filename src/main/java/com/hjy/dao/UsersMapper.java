package com.hjy.dao;

import com.hjy.entity.Users;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UsersMapper {

    Users findByUsersName(@Param("username")String username);
}
