package com.hjy.serivce;

import com.hjy.entity.Users;

import java.util.List;

public interface UsersService {

    Users findByUsersName(String username);
}
