package com.hjy.serivce;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * 自定义登陆检验规则
 */
public class CredentialMatcher extends SimpleCredentialsMatcher {

    //自定义验证规则
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken usernamePasswordToken=(UsernamePasswordToken) token;
        //session的密码
        String password =new String(usernamePasswordToken.getPassword());
        //数据库对应的密码
        String dePassword=info.getCredentials().toString();
        //返回对比结果
        return this.equals(password,dePassword);
    }
}
