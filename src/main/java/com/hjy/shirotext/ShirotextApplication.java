package com.hjy.shirotext;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
/*扫描括号内所有文件的注解并载入到Spring内*/
@ComponentScan("com.hjy.**")
/*Mybatis扫描*/
@MapperScan("com.hjy.dao")
public class ShirotextApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShirotextApplication.class, args);
    }
}
